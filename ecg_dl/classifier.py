import tensorflow as tf
from ecg_dl import read_data
import numpy as np
import os
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

from my_dl_lib import generic_dataset
from my_dl_lib import generic_neural_network
from my_dl_lib import generic_lstm
from my_dl_lib.generic_gan import gan
from my_dl_lib.generic_gan import vanila_gan
import logging


def ecg_classifier_cnn_model_fn(features, labels,  mode):
    window_size = 216
    # x_ecg_beats, y_tags = read_data.read_data_from_single_patient('100')

    # Input Layer
    # input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])
    input_layer = tf.reshape(features['x'], [-1, 1, window_size, 1])  # TODO: decide exactly shape and standart of input
    input_layer = tf.cast(input_layer, tf.float32)
    # Convolutional Layer #1
    num_of_filters_at_first_layer = 5
    kernel_length = 5
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=num_of_filters_at_first_layer,
        kernel_size=[1, kernel_length],
        padding="same",
        activation=tf.nn.relu)

    # Pooling Layer #1
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[1, 2], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    num_of_filters_at_second_layer = 10
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=num_of_filters_at_second_layer,
        kernel_size=[1, kernel_length],
        padding="same",
        activation=tf.nn.relu)
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[1, 2], strides=2)

    # Dense Layer (Dense == fully connected layer)
    reshpaed_size = (float(window_size) / 4.0) * num_of_filters_at_second_layer
    pool2_flat = tf.reshape(pool2, [-1, int(reshpaed_size)])

    # Dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
    dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)

    dropout = tf.layers.dropout(
        inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)
    # Our output tensor dropout has shape [batch_size, 1024]

    # Logits Layer
    logits = tf.layers.dense(inputs=dropout, units=2)

    # Our final output tensor of the CNN, logits, has shape [batch_size, 10]

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": tf.argmax(input=logits, axis=1),
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        # `logging_hook`.
        "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    # [1,0] ==> label is 0 ==> unormal beat
    # [0,1] ==> label is 1 ==> normal beat
    onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=2)
    loss = tf.losses.softmax_cross_entropy(onehot_labels=onehot_labels, logits=logits)

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def training_ecg_model():
    x_ecg_beats, y_tags = read_data.create_dataset_for_patient('100')

    # Some statistics:
    unormal_beats = [1 for x in y_tags if x == 0]
    print("Number of unormal beats: %d" % len(unormal_beats))
    # Create the Estimator
    ecg_classifier = tf.estimator.Estimator(
        model_fn=ecg_classifier_cnn_model_fn, model_dir="saved_models/ecg_cnn_model")

    '''
    Since CNNs can take a while to train, let's set up some logging so we can track progress during training. We can
    use TensorFlow's tf.train.SessionRunHook to create a tf.train.LoggingTensorHook that will log the probability values
     from the softmax layer of our CNN. Add the following to main()
    '''
    # Set up logging for predictions
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    # Train the model
    train_data = np.array(x_ecg_beats)
    train_labels = np.array(y_tags)

    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data},
        y=train_labels,
        batch_size=100,
        num_epochs=None,
        shuffle=True)

    ecg_classifier.train(
        input_fn=train_input_fn,
        steps=20000,
        hooks=[logging_hook])

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data},
        y=train_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = ecg_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_results)


def merge_data_of_patients(patient_numbers, binary_pred=True):
    """

    :param patient_numbers:
    :return:
    """
    first_iter = True
    merged_beats = np.array([])
    merged_tags = np.array([])
    for p in patient_numbers:
        print("Merging patient %s" % p)
        # In y_tags : 1 == Noraml, 0 == Unromal.
        x_ecg_beats, y_tags = read_data.create_dataset_for_patient(p, binary_pred=binary_pred)
        if first_iter:
            first_iter = False
            merged_beats = x_ecg_beats
            merged_tags = y_tags
        else:
            merged_beats = np.concatenate((merged_beats, x_ecg_beats), axis=0)
            merged_tags = np.concatenate((merged_tags, y_tags), axis=0)

    return merged_beats, merged_tags


def classify_all_patients(arch_file='nn_ecg_3_layer', logs_dir='./logs', mix_data_between_patients=True,
                          num_of_classes=2):
    """

    :param num_of_classes:
    :param mix_data_between_patients
    :param arch_file:
    :param logs_dir:
    :return:
    """

    orig_data_dir = os.path.join('Data', 'MIT database')
    patient_numbers = read_data.get_list_of_all_patient_numbers(orig_data_dir)
    train_set, validation_set, test_set = read_data.divide_data_to_train_val_test(patient_numbers)

    # Statistics of each set:
    # print("Train set statistics:")
    # read_data.get_statistics_of_beat_types(train_set)

    # print("Validation set statistics")
    # read_data.get_statistics_of_beat_types(validation_set)

    # print("Test set statistics")
    # read_data.get_statistics_of_beat_types(test_set)

    ecg_nn = generic_neural_network.GenericNeuralNetwork(arch_file, logs_dir)
    flag = 0
    i = 0
    print("Size of Train set: %d, Size of validation set: %d Size of Test set: %d"
          % (len(train_set), len(validation_set), len(test_set)))

    if num_of_classes > 2:
        validation_beats, validation_tags = merge_data_of_patients(validation_set, binary_pred=False)
    else:
        validation_beats, validation_tags = merge_data_of_patients(validation_set)

    dataset = generic_dataset.GenericDataSetIterator(validation_beats, validation_tags)
    validation_tags = dataset.convert_labels_to_one_hot_vector(num_of_classes)

    if mix_data_between_patients:
        train_beats, train_tags = merge_data_of_patients(train_set, binary_pred=(num_of_classes == 2))
        '''
        number_of_normal_beats = list(train_tags).count(1)
        number_unormal_beats = list(train_tags).count(0)
        print("Train set: %d Normal Beats and %d non-normal Beats" % (number_of_normal_beats,
                                                                          number_unormal_beats))
        '''
        dataset = generic_dataset.GenericDataSetIterator(train_beats, train_tags)
        # (1, 0) ==>  non-normal (0, 1) ==> Normal
        train_tags = dataset.convert_labels_to_one_hot_vector(num_of_classes)
        ecg_nn.train_tf(train_beats, train_tags, validation_beats, validation_tags,
                        1000, 100, "adam", "cross_entropy", unbalanced_ratio=None)

    if not mix_data_between_patients:
        for p in train_set:
            i += 1
            print("Training network on patient number %s" % p)
            # In y_tags : 1 == Noraml, 0 == Unromal.
            x_ecg_beats, y_tags = read_data.create_dataset_for_patient(p)

            number_of_normal_beats = list(y_tags).count(1)
            number_unormal_beats = list(y_tags).count(0)
            print("Patient %s has %d Normal Beats and %d non-normal Beats" % (p, number_of_normal_beats,
                                                                              number_unormal_beats))
            ratio_of_non_normal = float(number_of_normal_beats) / float(number_unormal_beats)
            coeffs = tf.constant([ratio_of_non_normal, 1])
            dataset = generic_dataset.GenericDataSetIterator(x_ecg_beats, y_tags)

            # (1, 0) ==>  non-normal (0, 1) ==> Normal
            y_one_hot_enocded = dataset.convert_labels_to_one_hot_vector(2)
            if flag == 0:
                # ecg_nn.train_tf(x_ecg_beats, y_one_hot_enocded, 1000, 150, "adam", "cross_entropy",
                #                unbalanced_ratio=None)
                ecg_nn.train_tf(x_ecg_beats, y_one_hot_enocded, validation_beats, validation_tags,
                                100, 25, "adam", "cross_entropy", unbalanced_ratio=None)
                flag = 1
            else:
                ecg_nn.train_existing_net_tf(x_ecg_beats, y_one_hot_enocded, validation_beats, validation_tags, 100, 25)
            print("Trained %d patients" % i)

    print("Training Done")

    print("Evaluation on test set:")
    test_beats, test_tags = merge_data_of_patients(test_set, binary_pred=(num_of_classes == 2))
    dataset = generic_dataset.GenericDataSetIterator(test_beats, test_tags)
    test_tags = dataset.convert_labels_to_one_hot_vector(num_of_classes)
    ecg_nn.eval_accuracy(test_beats, test_tags)

    probability_predictions_of_test_set = ecg_nn.get_probability_predictions(test_beats)
    binary_predictions_of_test_set = ecg_nn.get_binary_predictions(test_beats)

    if num_of_classes > 2:
        ecg_nn.calc_roc_for_multiclass_probelm(test_tags, probability_predictions_of_test_set)
        ecg_nn.calc_evaulations_for_multiclass(test_tags, probability_predictions_of_test_set,
                                               binary_predictions_of_test_set)
    # ecg_nn.calc_and_print_roc(test_tags, probability_predictions_of_test_set)
    # ecg_nn.print_precision_recall_graph(test_tags, probability_predictions_of_test_set)
    # ecg_nn.calculate_statistics(test_tags, binary_predictions_of_test_set,
    #                             "non-normal beats",
    #                             "normal beats", positive_index=0)
    '''
    print("Evaluating results on test set (Evaluation per patient)")
    y_one_hot_labels_of_all_patients = np.array([])
    full_predictions_of_all_patietns = np.array([])
    binary_predictions_of_all_patietns = np.array([])
    i = 0
    for p in test_set:
        i += 1
        print("Testing network on patient number %s" % p)
        x_ecg_beats, y_tags = read_data.create_dataset_for_patient(p)
        dataset = generic_dataset.GenericDataSetIterator(x_ecg_beats, y_tags)
        # (1, 0) ==>  non-normal (0, 1) ==> Normal
        y_one_hot_enocded = dataset.convert_labels_to_one_hot_vector(2)
        if i == 1:
            y_one_hot_labels_of_all_patients = y_one_hot_enocded
        else:
            y_one_hot_labels_of_all_patients = np.concatenate((y_one_hot_labels_of_all_patients, y_one_hot_enocded))
        ecg_nn.eval_accuracy(x_ecg_beats, y_one_hot_enocded)

        full_preds = ecg_nn.get_full_predictions(x_ecg_beats)
        bin_preds = ecg_nn.get_binary_predictions(x_ecg_beats)
        if i == 1:
            full_predictions_of_all_patietns = full_preds
            binary_predictions_of_all_patietns = bin_preds
        else:
            full_predictions_of_all_patietns = np.concatenate((full_predictions_of_all_patietns, full_preds))
            binary_predictions_of_all_patietns = np.concatenate((binary_predictions_of_all_patietns, bin_preds))
        id_pred_unormal = []
        for i, e in enumerate(bin_preds):
            if e[0] == 1:
                id_pred_unormal.append(i)
        num_true_unormals = 0
        for i in id_pred_unormal:
            if y_one_hot_enocded[i][0] == 1:
                num_true_unormals += 1

        total_beats_predicted_as_unromal = len(id_pred_unormal)
        print("predicted %d beats as non-nromal, among them %d were correct" %
              (total_beats_predicted_as_unromal, num_true_unormals))

        num_of_true_unormal_beats = len([1 for x in y_one_hot_enocded if x[0] == 1])
        id_of_true_unormal = []
        num_of_correct = 0
        for i,e in enumerate(y_one_hot_enocded):
            if e[0] == 1:
                id_of_true_unormal.append(i)
        for i in id_of_true_unormal:
            if bin_preds[i][0] == 1:
                num_of_correct += 1
        print("There are true %d unormal beats. among them %d were predicted correct" %
              (num_of_true_unormal_beats, num_of_correct))
        # print prec-rec:
        # ecg_nn.print_precision_recall_graph(y_one_hot_enocded, full_preds)
        # ecg_nn.calc_and_print_roc(y_one_hot_enocded, full_preds)
    print("Precision Recall of all patients:")
    ecg_nn.calc_and_print_roc(y_one_hot_labels_of_all_patients, full_predictions_of_all_patietns)
    ecg_nn.print_precision_recall_graph(y_one_hot_labels_of_all_patients, full_predictions_of_all_patietns)
    ecg_nn.calculate_statistics(y_one_hot_labels_of_all_patients, binary_predictions_of_all_patietns, "non-normal beats",
                                "normal beats", positive_index=0)
    '''
    '''
    for filename in os.listdir(data_dir):
        if filename.endswith("_tag.txt"):
            i += 1
            patient_number = filename.split("_")[0]
            print("Training network on patient number %s" % patient_number)
            x_ecg_beats, y_tags = read_data.create_dataset_for_patient(patient_number)
            dataset = generic_dataset.GenericDataSetIterator(x_ecg_beats, y_tags)
            y_one_hot_enocded = dataset.convert_labels_to_one_hot_vector(2)
            if flag == 0:
                ecg_nn.train_tf(x_ecg_beats, y_one_hot_enocded, 2000, 100, "adam", "cross_entropy")
                flag = 1
            else:
                ecg_nn.train_existing_net_tf(x_ecg_beats, y_one_hot_enocded, 2000, 100)
            print("Trained %d patients" % i)
    '''


def classify_all_patients_with_lstm(sequence_length=3):
    """

    :param sequence_length:
    :return:
    """
    orig_data_dir = os.path.join('Data', 'MIT database')
    patient_numbers = read_data.get_list_of_all_patient_numbers(orig_data_dir)

    train_set = patient_numbers[:int(0.7 * len(patient_numbers))]
    test_set = patient_numbers[int(0.7 * len(patient_numbers)):]
    ecg_lstm = generic_lstm.GenericLSTM(512, sequence_length, 240, 240, number_of_lstm_layers=1)
    flag = 0
    i = 0
    for p in train_set:
        i += 1
        print("Testing network on patient number %s" % p)
        x_ecg_beats, y_tags = read_data.create_dataset_for_patient(p)
        x_data, y_labels = read_data.convert_beats_for_lstm(x_ecg_beats, sequence_length=sequence_length)

        number_of_normal_beats = list(y_tags).count(1)
        number_unormal_beats = list(y_tags).count(0)
        print("Patient %s has %d Normal Beats and %d non-normal Beats" % (p, number_of_normal_beats,
                                                                          number_unormal_beats))

        ecg_lstm.train(x_data, y_labels, num_of_iterations=1000, batch_size=100, display_step=300, learning_rate=0.001,
                       loss_function='mse', optimizer='rms_prop', word_to_index=None, index_to_word=None)
        # Just for testing:
        x_test = x_data[:5]
        y_test = y_labels[:5]
        y_pred = ecg_lstm.session.run(ecg_lstm.V, feed_dict={ecg_lstm.x: x_test})
        plt.figure()
        plt.xlabel("time")
        plt.ylabel("voltage")
        plt.plot(y_test[0], label='truth')
        plt.plot(y_pred[0], label='pred')
        plt.show()
        '''
        if flag == 0:
            ecg_nn.train_tf(x_ecg_beats, y_one_hot_enocded, 1000, 150, "adam", "cross_entropy",
                            unbalanced_ratio=coeffs)
            flag = 1
        else:
            ecg_nn.train_existing_net_tf(x_ecg_beats, y_one_hot_enocded, 1000, 150)
        '''
        print("Trained %d patients" % i)

    print("Evaluating results on test set (Evaluation per patient)")


def test_lstm():
    """

    :return:
    """
    orig_data_dir = os.path.join('Data', 'MIT database')
    patient_number = '100'
    time, voltage1, voltage2, tags_time, tags, r_peaks_indexes = read_data.read_data_from_single_patient(patient_number)
    my_lstm = generic_lstm.GenericLSTM(512, 5, 20)
    x_data, y_labels = my_lstm.prepare_data(voltage1)
    dataset = generic_dataset.GenericDataSetIterator(x_data, y_labels)
    x_batch, y_batch = dataset.next_batch(6)
    print(x_batch)
    print(y_batch)


def train_ecg_gan(generated_class=1, model_name='gan'):
    """

    :return:
    """
    ecg_gan = gan.GenericGAN(discriminator_input_height=240, discriminator_input_width=1,
                             discriminator_number_of_input_channels=1,
                             discriminator_number_of_filters_at_first_layer=64,
                             discriminator_arch_file=None, generator_input_dim=10, generator_output_height=240,
                             generator_output_width=1, generator_output_channels=1, generator_architecture_file=None,
                             tensor_board_logs_dir='./logs/gan/' + model_name, is_input_an_image=False,
                             checkpoint_dir='./saved_models/gan_normal/' + model_name,
                             generated_samples_dir='GAN_samples/' + model_name)

    orig_data_dir = os.path.join('Data', 'MIT database')
    patient_numbers = read_data.get_list_of_all_patient_numbers(orig_data_dir)
    train_set, validation_set, test_set = read_data.divide_data_to_train_val_test(patient_numbers)

    # validation_beats, validation_tags = merge_data_of_patients(validation_set, binary_pred=False)
    train_beats, train_tags = merge_data_of_patients(train_set, binary_pred=False)
    filterd_beats, filterd_tags = read_data.get_all_beats_of_type(generated_class, train_beats, train_tags)

    # We want to do 25 epochs:
    length_of_data = len(filterd_beats)
    num_of_iters_to_complete_one_epoch = length_of_data / 128
    num_of_iters_to_complete_25_epcohes = int(num_of_iters_to_complete_one_epoch * 25)
    print("Running %d iterations", num_of_iters_to_complete_25_epcohes)
    ecg_gan.train(discriminator_x_train=filterd_beats, discriminator_y_train=filterd_tags,
                  num_of_iterations= num_of_iters_to_complete_25_epcohes, batch_size=64, learning_rate=0.0002,
                  momentum_term_adam=0.5)


def train_ecg_vanila_gan(model_name='vanila_gan', generated_class=1):
    """
    Train a very simple gan (we will call vanila gan)
    :param model_name:
    :param generated_class:
    :return:
    """
    ecg_gan = vanila_gan.VanilaGan(x_dim=240, model_name=model_name)
    orig_data_dir = os.path.join('Data', 'MIT database')
    patient_numbers = read_data.get_list_of_all_patient_numbers(orig_data_dir)
    train_set, validation_set, test_set = divide_data_to_train_val_test(patient_numbers)

    # validation_beats, validation_tags = merge_data_of_patients(validation_set, binary_pred=False)
    train_beats, train_tags = merge_data_of_patients(train_set, binary_pred=False)
    filterd_beats, filterd_tags = read_data.get_all_beats_of_type(generated_class, train_beats, train_tags)
    ecg_gan.train(filterd_beats, filterd_tags)

if __name__ == "__main__":
    logging.root.setLevel(logging.DEBUG)
    tf.logging.set_verbosity(tf.logging.INFO)

    # 1. With 1 layer of 2 neruons :
    # classify_all_patients(os.path.join('architecture_files', 'nn_ecg_3_layer'),
    #                      logs_dir=os.path.join("logs", "one_layer"), num_of_classes=2)

    # 2. With 3 layers:
    # classify_all_patients(os.path.join('architecture_files', 'nn_ecg_3_layer'),
    #                      logs_dir=os.path.join("logs", "three_layer"))

    # 3. With 1d-conv :
    # classify_all_patients(os.path.join('architecture_files', 'nn_ecg_1d_conv'),
    #                      logs_dir=os.path.join("logs", "conv_1d"))

    # 3. With LSTM:
    # classify_all_patients_with_lstm(3)

    # Multiple Classes:

    # 1. With 1 layer of 5 neurons:
    # classify_all_patients(os.path.join('architecture_files', 'multiple_classes', 'nn_ecg_3_layer'),
    #                       logs_dir=os.path.join("logs", 'multiple_classes', "one_layer"), num_of_classes=5)

    # 3. With 1d conv:

    # classify_all_patients(os.path.join('architecture_files', 'multiple_classes', 'nn_ecg_1d_conv'),
    #                      logs_dir=os.path.join("logs", 'multiple_classes', "one_layer"), num_of_classes=5)

    # Test Discriminator :
    '''
    ecg_discriminator = discriminator.GenericDiscriminator(input_height=240, input_width=1, number_of_input_channels=1,
                                             number_of_filters_at_first_layer=64, architecture_file=None,
                                             tensor_board_logs_dir='./logs')
    # train_set = ecg_data.train()
    x_ecg_beats, y_tags = read_data.create_dataset_for_patient('100', binary_pred=True)
    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        # reshape_tensor = tf.reshape(input_to_layer, [-1, layer[1], layer[2], layer[3]])
        test_input = x_ecg_beats[0:3]
        gen_output = ecg_discriminator.run_discriminator(sess, test_input)
        print("DONE")
    '''

    # GAN :
    train_ecg_gan(generated_class=4, model_name='gan_batch_128_iters_1000000_gen_input_dim_100')

    # train_ecg_vanila_gan(model_name='ecg_vanila_gan_batch_128_iters_1000000_input_dim_gen_100')
